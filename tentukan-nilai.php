<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Nomor 4</title>
</head>
<body>
<?php

/*
Soal 4
buatlah sebuah file dengan nama tentukan-nilai.php. Di dalam file tersebut buatlah function dengan nama tentukan_nilai yang menerima parameter 
berupa integer. dengan ketentuan jika paramater integer lebih besar dari sama dengan 85 dan lebih kecil sama dengan 100 maka akan mereturn String “Sangat Baik” 
Selain itu jika parameter integer lebih besar sama dengan 70 dan lebih kecil dari 85 maka akan mereturn string “Baik” selain itu jika parameter number lebih besar 
sama dengan 60 dan lebih kecil dari 70 maka akan mereturn string “Cukup” selain itu maka akan mereturn string “Kurang”
*/

// Code function di sini
echo "<h3>Soal No 4 Tentukan Nilai</h3>";

function tentukan_nilai($int){
    if($int >= 85){
        return "Sangat Baik<br>";
    } else if($int <=84 && $int >= 60){
        return "Baik<br>";
    } else if($int <=69 && $int >= 70){
        return "Cukup<br>";
    } else {
        return "Kurang<br>";
    }
    }
// Hapus komentar di bawah ini untuk jalankan code
echo tentukan_nilai(98); //Sangat Baik
echo tentukan_nilai(76); //Baik
echo tentukan_nilai(67); //Cukup
echo tentukan_nilai(43); //Kurang

?>
</body>
</html>